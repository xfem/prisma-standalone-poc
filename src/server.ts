require('dotenv').config();
const { graphqlHTTP } = require("express-graphql");
const { loadSchema } = require('@graphql-tools/load');
const { UrlLoader } = require('@graphql-tools/url-loader');
/*
const { createHttpLink } = require("apollo-link-http");
const { ApolloClient } = require("apollo-client");
const { InMemoryCache } = require("apollo-cache-inmemory"); */
const { PrismaClient } = require('@prisma/client');

const prisma = new PrismaClient({
  log: [{ level: 'query', emit: 'event' }],
});
prisma.$on('query', e => {
  console.log(e)
});

// get the auth scheme and pull the schema
async function getSchema() {
  // maye need to install node types for this
  const awkward = process.env.HASURA_GRAPHQL_ADMIN_SECRET;
  const myUrlHasura = process.env.HASURA_FRONTEND;
  const schema = await loadSchema( `${myUrlHasura}/v1/graphql`, {
    loaders: [
      new UrlLoader(),
    ],
    headers: {
      Accept: 'application/json',
      'x-hasura-admin-secret': `${awkward}` // don't pass this from the frontend, obviously
    },
    method: 'GET'
  }).then((schema) => {

    console.log(schema)
    // return getExpress(schema)
  })
    .catch((error) => {
    console.log("fetching schema")
    console.error(error)
  })
}

async function main() {

  const schema = getSchema()
    .catch((error) => { console.error(error) })

}
main();

function getExpress(schema) {
  const app = require("express");
  app(4000, () => {
    graphqlHTTP({
      schema: schema,
      graphiql: true,
    }).catch((error) => { console.log(error) }
    )

    app.listen(4000, () => {
      console.info(`Server listening on http://locahlost:4000`)
    });
  })
}
