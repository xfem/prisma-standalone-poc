// this example uses nodemon and pulls all the scroes available for a user.
// its' sorted by the score max() and the top score is plucked giving us a winner.
// super basic for now
//require("sucrase/register");

require('dotenv').config()


const { PrismaClient } = require('@prisma/client')  

// cached client
//import prisma from './client'

const prisma = new PrismaClient({
  log: [{ level: 'query', emit: 'event' }],
})
prisma.$on('query', e => {
  console.log(e)
})
async function main() {
  const allUsers = await prisma.users_scores.findMany({
          orderBy: [{ score: 'desc' }]
        }).then((users_scores) => {
      console.error(users_scores)
      })
}
let users = main();
